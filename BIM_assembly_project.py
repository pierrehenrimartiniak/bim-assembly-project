#--------------------------BIM-PROJET-ASSEMBLEUR-------------------------------

#    Noms:         Prénoms:

#    Després       Lenaïg          (*)
#    Gonneau       Yoann           (*)
#    Martiniak     Pierre-Henri    (*)
#    Merel         Laurie          (*)

#    (*) contributions égales, les noms sont donnés par ordre alphabétique

#    NB: modifier la ligne 70 selon le nom du fichier contenant les reads

#------------------------------LES-FONCTIONS-----------------------------------

#Fonction permetant de chercher au sein d'une liste de chaines de caracteres L, la chaine de caractere la plus
#longue appelee maxseq (ce sera le contig), et retournant maxseq au format list apres l'avoir supprimee de la liste L initiale.
#Renvoyer [maxseq] au format list permet de pouvoir concatener [maxseq] a une autre liste "contig" introduite plus loin.
def max_length(L):
    maxlen=0 #On introduit la variable maxlen qui correspond à la longueur la plus grande. Initialement elle vaut 0
    maxseq=0 #On introduit la variable maxseq qui correspond à la séquence avec la longueur la plus grande. 
    for i in L:  #i correspond à une séquence (prises les unes après les autres) dans notre liste de read L
        if len(i)>maxlen: #Si la longueur de la séquence i est plus grande que maxlen précédent
            maxlen=len(i)  #Alors maxlen va prendre la valeur de la longeur de la séquence i 
            maxseq=i  #Et on stock dans maxseq cette séquence i
    L.remove(maxseq) #Une fois la plus grande séquence de toutes trouvée, on l'enlève de la liste de read
    return([maxseq])  #crochet pour renvoyer la séquence sous forme de liste 

#fonction permetant de trouver le meilleur alignement i entre l'extremite 3' d'une sequence de reference et
#l'extremite 5' d'une autre sequence test plus courte
def comp_seq_3(seq_ref,seq_test):
    for i in range(len(seq_test),0,-1): #i correspond à l'alignement (qui peut être correct ou incorect): il prend comme valeur initiale la longueur de la séquence test et diminue de 1 à chaque boucle jusqu'à ce que la condition de la ligne 23 soit remplie mais n'ira pas plus bas que 1
        if seq_test[0:i]==seq_ref[len(seq_ref)-i:]: #Si la séquence test de la position 0 à la position i-1  correspond à la séquence ref de la position -i à -1
            return(i) #Alors on retrourne l'alignement 
    return(0) #si on est arrivé à la fin de la boucle il n'y a pas eu d'alignement sur lequel ça match, alors on retourne 0

"""
exemple:
            -7    -1          -6   -1           -5  -1
seq_ref   TAATGCTGA        TAATGCTGA        TAATGCTGA
              i=7      ->        i=6     ->     |||||    => ça match! on retourne i=5
seq_test    GCTGATA           GCTGATA           GCTGATA
            0     6           0    5            0   4
"""

#fonction permetant de trouver le meilleur alignement i entre l'extremite 5' d'une sequence de reference seq_ref et
#l'extremite 3' d'une autre sequence test plus courte
def comp_seq_5(seq_ref,seq_test):
    for i in range(len(seq_test),0,-1): #i correspond à l'alignement (correct ou incorect), comme pour comp_seq_3
        if seq_test[len(seq_test)-i:]==seq_ref[0:i]: #Si la séquence test de la position -i à la position -1  correspond à la séquence ref de la position 0 à i-1
            return(i) #Alors on retrourne l'alignement 
    return(0) # si la condition précedente n'est pas remplie alors que i=1, la boucle se termine et la fonction renvoie 0

"""
exemple:
              0  3             0 2              01                     0
seq_ref       TAATGCTGA        TAATGCTGA        TAATGCTGA              TAATGCTGA       
               i=4             i=3              i=2                    i=1        => ça ne match pas... => la boucle est terminée et retourne 0
seq_test      GCTG            GCTG            GCTG                  GCTG             
              -4 -1            -3               -2                     -1
"""


#----------------------------L'INITIALISATION----------------------------------
# 1. On ouvre tout d'abord le fichier contenant l'ensemble des reads appele "my_read.txt" (adresse relative) qui doit se trouver au meme
#endroit que l'assembleur.
# 2. Puis la fonction read permet d'en extraire une chaine de caratere "brute"
# 3. Enfin, la fonction splitlines permet de scinder, a chaque retour a la ligne dans le fichier  "my_read.txt", cette chaine de caractere
#"brute" en plusieurs chaines de caracteres (les reads) que l'on stocke dans la liste appelée seq.
seq=open("read.txt").read().splitlines()
#On cree ensuite la liste contig qui ne contiendra au depart pour seul element, que la chaine de caractere la plus longue de la liste seq
#Cette plus longue chaine de caractere aura ete trouvee grace a la fonction max_length introduite plus haut.
contig=max_length(seq)

#--------------------------------L'ASSEMBLAGE-----------------------------------


while seq: #Tant que nous avons des séquences dans la liste seq i.e. tant que la liste seq n'est pas vide 
    best_overlap=0 #On définit une variable correspondant au meilleur alignement. Au début elle vaut 0
    best_read=0 #On définit une variable qui correspond à la séquence avec le meilleur alignement. Au début on a pas de séquence (0)
    #on compare, les unes après les autres, les sequences de seq avec le dernier contig de la liste contigs (au début il n'y a qu'un seul contig donc c'est bien le dernier...)
    for i in seq: #i correspond à une séquence dans seq 
        overlap_droite=comp_seq_3(contig[-1],i)  #l'alignement à droite correspond à la comparaison en 3' de la séquence ref(contig[-1]) avec la séquence test(i)
        overlap_gauche=comp_seq_5(contig[-1],i) #L'alignement à gauche correspond à la comparaison en 5' de la séquence ref(contig[-1]) avec la séquence test(i) 
        if max(overlap_droite,overlap_gauche)>best_overlap: #On prend la valeur max des overlap qu'on compare au best_overlap définit précedement. Et si il y a un des best_overlap qui est supérieur au best_overlap définit précédemment 
            best_read=i #alors i devient la séquence avec le meilleur alignement et:
            if overlap_droite>=overlap_gauche:  #Si l'alignement à droite est meilleur ou pareil que l'alignement à gauche (si il est pareil, on décide arbitrairement de prendre celui de droite)
                best_overlap=overlap_droite #Alors le meilleur alignement est l'alignement à droite 
                coller="droite" #On définit une variable qui nous dis où on doit coller la séquence test sur la séquence ref, dans le cas présent on la colle à droite de seq_ref 
            else:  #Si l'alignement à gauche est meilleur que l'alignement à droite 
                best_overlap=overlap_gauche #Alors le meilleur alignement est l'alignement à gauche 
                coller="gauche" #On crée alors une variable qui nous dit où on doit coller la séquence test sur la séquence de référence, dans le cas présent on la colle à gauche de seq_ref
    if best_overlap>0: #Une fois qu'on a comparé toutes les séquences de seq, si le meilleur alignement est plus grand que 0 (sinon c'est qu'il n'y a pas d'alignement correct) alors:
        if coller=="droite": #Si précéédemment on a trouvé que l'on devait coller la séquenece test à droite du contig 
            contig[-1]=contig[-1]+best_read[best_overlap:] #On insère la séquence test à droite de la séquence de référence
        elif coller=="gauche": #Si précédemment on a trouvé qu'on devait coller la séquence test à gauche du contig 
            contig[-1]=best_read[0:-best_overlap]+contig[-1] #On insère la séquence test à gauche de la séquence de référence 
        seq.remove(best_read) #On enlève cette séquence ajoutée de la liste de read 
    elif best_overlap==0 and seq!=[]:  #Si on trouve pas d'alignement et que seq n'est pas vide, il reste encore des sequences...
        contig=contig+max_length(seq)  #...donc on va créer un nouveau contig: on ajoute dans la liste de contig une nouvelle séquence qui correspond à la plus grande séquence restant dans la liste de read. Ce nouveau contig se retrouvera en position [-1] de la liste contig. Donc la suite de l'assemblage se fera dessus.

#---------------------------------------LES-RESULTATS-------------------------

print(contig)

#-------------------------------LIMITES --------------------------------------

#Lorsque les alignements à droite et à gauche du contig sont égaux, on prend par défaut l'alignement à droite. En toute rigueur, il serait mieux
#de renvoyer les deux contigs possibles: l'un en collant à droite, l'autre en collant à gauche. Cette décision n'a pas été appliquée ici car on est là
#dans un cas très particulier
#et car ça aurait beaucoup compliqué le programme: il ne s'agit pas seulement de rajouter les deux contigs dans la liste de contig, car selon qu'on colle à
#droite où à gauche, la liste de read ne sera pas la même après.

#Lors d'un séquencage, les polymerases utilisées peuvent commettre des erreurs (mutations): se tromper de base(mésapariement) ou des insertions/deletions de
#base(indel). Ce programme ne tient pas compte des mutations possibles. En effet, la prise en compte de ces erreurs fait appel à des notions de programation dynamique
#et aurait ralentitsignificativement l'execution du programme. Nous avons donc décidé de réaliser un second programme qui tient compte des mutations potentielles grâce
#à l'algorithme de Needleman-Wunsch.







#Avec l'algorithme de Needleman-Wunch

#--------------------------BIM-PROJET-ASSEMBLEUR-------------------------------

#    Noms:         Prénoms:

#    Després       Lenaïg          (*)
#    Gonneau       Yoann           (*)
#    Martiniak     Pierre-Henri    (*)
#    Merel         Laurie          (*)

#    (*) contributions égales, les noms sont donnés par ordre alphabétique

# Ce scrit permet de tenir compte des éventuelles mutations annocées dans la partie "LIMITES" de l'autre script. Pour cela, au lieu de demander une corespondance parfaite des deux sequences dans
#les fonctions comp_seq_3 et comp_seq_5, on va determiner le meilleur alignement possible et lui attribuer un score. Si ce score est supérieur à une valeur déterminée empiriquement,
#on va considéré qu'il y a une corespondance parfaite entre les deux sequences.
#Pour ce faire, nous utiliserons l'algorithme de Needleman-Wunch

#------------------------------LES-FONCTIONS-----------------------------------

#Fonction qui permet de produire une "matrice" (sous forme d'une liste de liste) avec la sequence seq1 en colonne et la séquence seq2 en ligne:
def scoring_matrix(seq1,seq2): 
    n1=len(seq1)+1 #Nombre de colonne qui correspond à la longueur de seq1 + la première colonne
    n2=len(seq2)+1  #Nombre de lignes qui correspond à la longuueur de seq2 + la première ligne
    M = [ [ [0,""] for i in range(n1) ] for j in range(n2) ]
    #On introduit la variable M (correspondant à la matrice). Chaque case de la matrice prend la forme d'une liste commencant par un
    #nombre entier suivi d'une chaine de caractères indiquant comment a été calculé cette valeur ("up si calculée à partir de la case au dessus, "left si calculée à partir de la case
    #à gauche, "up_left" si calculée à partir de la case en haut à gauche, ces directions sont assimilées à des flèches pointant vers la case indiquée). Ces cases seront donc de la forme [nombre,"direction"]. Initialement la matrice est vide et toutes les cases sont de la
    #forme [0,""]. Dans ce cas la valeur M[ligne][colonne][0] de la liste est 0 et la valeur M[ligne][colonne][-1] de la liste est une chaine de caractère vide "".
    
    #Exemple avec seq1=ATGC et seq2=ATCGT
    #On a alors n1=5 (5 colonnes) et n2=6 (6 lignes)
    #La matrice M est créée et chaque case prend [0,""] comme valeur 
    """
                      A         T         G        C        (seq1)
                   
       [[ [0,""] , [0,""] ,  [0,""]  , [0,""] ,  [0,""] ],
        
    A   [ [0,""] , [0,""] ,  [0,""]  , [0,""] ,  [0,""] ],
    
    T   [ [0,""] , [0,""] ,  [0,""]  , [0,""] ,  [0,""] ],
    
    C   [ [0,""] , [0,""] ,  [0,""]  , [0,""] ,  [0,""] ],
     
    G   [ [0,""] , [0,""] ,  [0,""]  , [0,""] ,  [0,""] ],
    
    T   [ [0,""] , [0,""] ,  [0,""]  , [0,""] ,  [0,""] ]]
 (seq2)
    """
    #Pour remplir la matrice, nous allons donner des nombres et directions arbitraires à la première ligne et à la première colonne (lignes 85 à 88):
    
    """
                         A              T              G              C        (seq1)
                   
       [[ [0,""]    , [-1,"left"]  ,  [-2,"left"]  , [-3,"left"] ,  [-4,"left"] ],
        
    A   [ [-1,"up"] , [0,""]       ,  [0,""]       , [0,""]      ,  [0,""]      ],
    
    T   [ [-2,"up"] , [0,""]       ,  [0,""]       , [0,""]      ,  [0,""]      ],
    
    C   [ [-3,"up"] , [0,""]       ,  [0,""]       , [0,""]      ,  [0,""]      ],
     
    G   [ [-4,"up"] , [0,""]       ,  [0,""]       , [0,""]      ,  [0,""]      ],
    
    T   [ [-4,"up"] , [0,""]       ,  [0,""]       , [0,""]      ,  [0,""]      ]]
 (seq2)
    """  
    
    #Puis nous allons parcourir la matrice ligne par ligne et pour chaque case, calculer 3 valeurs:
    # - gap1: correspondant à une deletion dans seq1 ou une insertion dans seq2: c'est la valeur de la case du dessus à laquelle on soustrait 1 
    # - gap2: correspondant à une deletion dans seq2 ou une insertion dans seq1: c'est la valeur de la case de gauche à laquelle on soustrait 1
    # - align correspondant à l'alignement de seq1 et seq2 : c'est la valeur de la case en haut à gauche à laquelle on ajoute ou soustrait 1 si il y a respectivement concordance (dans cette exemple pour la case M[1][1] on a "A" pour seq1 et "A" pour seq2)
    #   ou dicordance (dans cet exemple pour la case M[1][2], on a "T" pour seq1 et "A" pour seq2) des deux bases
    #on donnera à cette case la valeur maximale parmis gap1, gap2, et align en indiquant respectivement les directions "up", "left", et "up_left"
    #dans le cas où les valeurs maximales sont obtenues pour gap1 et align ou bien gap1 gap2 et align ou tout autre combinaison, on indique ces différentes directions. Selon les cas, la case sera donc de la forme
    # [nombre, "up", "left", "up_left"] en respectant cet ordre. Il y a alors plusieurs possibilités de séquences à renvoyer, pour des raisons de simplicité, nous avons décidé de
    #n'en renvoyer qu'une selon un ordre de priorité arbitraire: "up_left" > "left" > "up" ce qui correspond à M[ligne][colonne][-1] dans tous les cas.

    for l in range(0,n2): #On parcourt la matrice M lignes par lignes 
        for c in range(0,n1): #Puis on parcourt la matrice M colonnes par colonnes => la matrice sera remplie selon un sens de lecture occidental
            if c>0 and l>0:#si le n° de la colonne (c) ET le n° de la ligne (l) sont >0 i.e. si on n'est pas dans la 1ere ligne ou dans la 1ere colonne
                gap1=M[l-1][c][0]-1#On introduit une variable gap1 qui est égale à la valeur [0] de la liste se trouavnt dans la case du dessus à laquelle on enlève 1 
                gap2=M[l][c-1][0]-1#On introduit une variable gap2 qui est égal à la valeur [0] de la liste se trouvant dans la case juste à gauche à laquelle on enlève 1
                
                if seq2[l-1]==seq1[c-1]:   #Si il y a concordance des deux bases pour la case considérée
                    align=M[l-1][c-1][0]+1    #Alors on introduit la variable align égale à la valeur [0] de la liste se trouavnt dans la case en haut à gauche à laquelle on ajoute 1 car dans ce cas là, on a une concordance entre les 2 séquenecs 
                else:                      #Si il y a discordance des deux bases pour la case considérée
                    align=M[l-1][c-1][0]-1    #Alors on introduit la variable align égale à la valeur [0] de la liste se trouvant dans la case en haut a gauche a laquelle on soustrait 1 car dans ce cas là, on a une discordance entre les 2 séquences 
                M[l][c]=[max(align,gap1,gap2)]    #Ainsi, la valeur dans la case dans laquelle on se trouve prend alors la valeur maximale parmis les 3 calculées gap1, gap2, et align(qui est sois une concordance, soit une discordance selon les cas): à ce moment là, la case est de la forme [nombre] et plus [0,""]
                
                #Maintenant, on va s'intéresser à la valeur [-1] des listes se trouvant dans les cases 
                #si la valeur maximale que l'on vient d'attribuer est celle de gap1 on rajoute dans la case une flèche vers le haut("up")
                #si cette valeur est égale à gap2 on met une flèche vers la gauche("left")
                #si elle est égale à align on met une flèche en diagonale ("up_left")
                if M[l][c][0]==gap1:
                    M[l][c]=M[l][c]+["up"]        # => la case sera de la forme [nombre, "up"]
                if M[l][c][0]==gap2:
                    M[l][c]=M[l][c]+["left"]      # => la case sera de la forme [nombre, "left"] ou [nombre, "up", "left"] si il y a des ex aequo
                if M[l][c][0]==align:
                    M[l][c]=M[l][c]+["up_left"]   # => la case sera de la forme [nombre, "up_left"] ou bien si il y a des ex aequo: [nombre, "up","up_left"] ou [nombre,"left","up_left"] ou [nombre, "up", "left", "up_left"]
                #on peut noter que remplir les cases de cette façon permet de respecter la priorité arbitraire définie précedement
                
            elif c>0:#Sinon, si le n° des colonnes SEULEMENT est supérieur à 0, donc qu'on est dans la toute première ligne de la matrice (mais pas dans la toute première case..) 
                M[l][c]=[-c, "left"] #Alors la liste de la cae dans laquelle on se trouve prend la valeur [-c,"left"]: la première ligne sera donc bien de la forme 0, -1 , -2, etc...
            elif l>0: #Sinon, si ne l° des lignes SEULEMENT est supérieur à 0, donc qu'on est dans la toute première colonne de la matrice (mais pas dans la toute première case...)
                M[l][c]=[-l , "up"] #Alors la liste de la case dans laquelle on se trouve prend la valeur [-l,"up"]: la première colonne sera donc bien de la forme 0, -1, -2, -3, etc...
                
                
    """
dans l'exemple donné on obtient cette matrice:
    
                               A                                   T                    G                                 C                          (seq1)
                               
    [  [ [0, '']  ,          [-1, 'left']                  ,     [-2, 'left']   ,     [-3, 'left']                  ,   [-4, 'left']         ],
      
      
   A   [ [-1, 'up'],         [1, 'up_left']               ,      [0, 'left']   ,      [-1, 'up', 'left', 'up_left'],    [-1, 'up', 'up_left']],
   
     
   T   [ [-2, 'up'],         [0, 'up']                    ,      [2, 'up_left'],      [1, 'left']                  ,    [0, 'left']          ],
   
      
   C   [ [-3, 'up'],         [-1, 'up', 'left', 'up_left'],      [1, 'up']     ,      [1, 'up_left']               ,    [2, 'up_left']       ],
   
     
   G   [ [-4, 'up'],         [-1, 'left', 'up_left']      ,      [0, 'up']     ,      [2, 'up_left']               ,    [1, 'up', 'left']    ],
   
     
   T   [ [-5, 'up'],         [-1, 'left', 'up_left']      ,      [0, 'up_left'],      [1, 'up']                    ,    [1, 'up_left']       ]     ]
(seq2)
                
    """
    return(M)  #la fonction renvoie la matrice générée

#Cette fonction va permettre de renvoyer la séquence seq1/seq2 mais en changeant là où il y a des mésappariements et là où il y a des indel:
#etant donné qu'une mutation peut se trouver sur seq1 ou sur seq2, à chaque mutation il y a deux sequences possibles à renvoyer. Pour simplifier on dira que: 
#-pour un mesapariement, on dira de façon arbitraire que la mutation porte sur seq1 et donc on prendra la base de seq2
#-pour les indel, on dira de façon arbitraire qu'il s'agit d'une déletion
def matrix_to_newseq(M,seq1,seq2):
    newseq=""   #tout d'abord on initialise cette nouvelle sequence (appellée newseq) sous la forme d'une chaine de caractère ne contenant rien au départ
    #on va ensuite parcourir la matrice M créée précedement à partir de la case M[-1][-1] (donc à partir de la case en bas à droite) jusqu'à la case M[0][0] (donc la case en haut à gauche) en suivant le chemin tracé par les directions des cases M[ligne][colonne][-1] (d'après la priorité arbitraire...) 
    #pour cela on initialise 2 variables li (ligne ) et co (colonne) respectivement égales au nombre de ligne -1 (donc à len(seq2)) et au nombre de colonne -1 (donc à len(seq1)) , soit li=5 et co=4 dans l'exemple précedent
    #ces 2 variable serviront d'indice des chaines de caractère seq1 et seq2 e.g. seq1[co-1]="G" lorsque co=3 (4eme colonne du tableau, co=3 , et seq1[co-1] = seq1[2])
    co=len(seq1)         #co=4 dans l exemple    donc seq1[co-1]="C" 
    li=len(seq2)         #li=5 dans l'exemple    donc seq2[li-1]="T"    et M[5][4]= M[-1][-1] au départ(dernière case de la matrice)
    while li>0 or co>0:  #tant que li OU co sont strictement >0 i.e. tant qu'on n'a pas li=0 et co=0  i.e. tant qu'on n'est pas dans la toute première case de la matrice: quand on y arrivera, on aura fini de parcourir la matrice et on aura notre "newseq"
        if M[li][co][-1]=="up_left" and seq2[li-1]==seq1[co-1]: #si la direction prioritaire de la case dans laquelle on est (indiquée par [-1] et pas [0] comme précedement) est une flèche en diagonale et que les bases des 2 sequences sont les mêmes (concordance)
            newseq=seq1[co-1]+newseq       #on prend cette base identique pour seq1 et seq2 et on la colle a gauche de newseq 
            li=li-1      #on passe a la ligne du dessus 
            co=co-1      #On passe à la colonne d'avant => on se retrouve dans la case indiquée par la directino "up_left"
        if M[li][co][-1]=="up_left" and seq2[li-1]!=seq1[co-1]: #si la direction prioritaire est up_left mais que les bases sont différentes (discordance)
            newseq=seq2[li-1]+newseq   #pareil: on colle la base a gauche de newseq, par convention on décide de prendre la base de seq2(qui est diferente de celle de seq1)
            co=co-1   #on passe a la colonnes d'avant 
            li=li-1   #On passe à la ligne du dessus  => on se retrouve dans la case indiquée par la directino "up_left"
            #dans l'exemple précedent on commence à li=5 et co=4 , M[5][4]=[1, 'up_left'] et  M[5][4][-1]="up_left"
            #puisque seq2[5-1]="T"  et  seq1[4-1]="C" sont différents, on colle la base de seq2(par convention) qui est "T" a gauche de newseq
            #newseq="" au départ donc on obtient seq2[li-1]+newseq="T"
            # avec co=co-1 et li= li-1 on aura après cette étape li=4 et co=3 et donc on sera dans la case juste au dessus a gauche(    [2, 'up_left']   )
        if M[li][co][-1]=="up":
            #si on a une flèche vers le haut on prend la base de seq2 et on la colle a gauche de newseq(par convention on dit qu'il y a eu déletion de cette base et pas insertion)
            newseq=seq2[li-1]+newseq
            li=li-1#permet de passer a la ligne au dessus
            #pour l'exemple précedent, quand li=3 et co=3 , M[li][co]=[1, 'up'] donc M[li][co][1]=="up"
            #newseq="GT" et on ajoute la base de seq2[li-1] qui est "C" => newseq="CGT"
                              
        if M[li][co][1]=="left":
            newseq=seq1[co-1]+newseq    #pareil mais la flèche va vers la gauche donc on prend la base de seq1
            co=co-1 #permet de passer a la colonne a gauche

    return(newseq)  #a la fin on aura newseq="ATCGT" ; # NB: pour certaines cases il y a plusieurs possibilités e.g. M[1][3]=[-1, 'up', 'left', 'up_left'] , on prend par convention M[1][3][-1]="up_left"   (on ne passe pas par cette case dans notre exemple cependant)


#Cette fonction permet de trouver le meilleur alignement entre l'extrémité 3' de la séquence de référence et l'extrémité 5' d'une séquence test:
def comp_seq_3(seq_ref,seq_test): 
    for i in range(len(seq_test),0,-1):  #i correspond à l'alignement 
        M=scoring_matrix(seq_test[0:i],seq_ref[-i:]) #On rempli la matrice pour les 2 séquences
        score=M[-1][-1][0] #Le score de l'alignement correspond à la valeur qui se trouve dans la case en bas à droite en position [0] de la liste 
        if score>=i-4:    #si le score est sufisament élevé (i-4 déterminé de façon empirique) on dit que seq_ref match avec seq_test et on arrête le décalage de seq_test par rapport à seq_ref à cet endroit là
            return(M)     #On renvoit alors la matrice corespondante (au lieu de renvoyer une séquence on renvoie la matrice)
    return([[0]]) #si pour toutes les valeurs de i prises, aucun score n'est sufisament élevé, on renvoie également une liste de liste mais avec une seule valeur de telle sorte que si on cherche le l'overlap sur cette liste de liste, ça renvoie 0. Ca ne marcherait pas si on faisait directement return(0)

#Cette fonction permet de trouver le meilleur alignement entre l'extrémité 5' de la séquence de référence et l'extrémité 3' d'une séquence test:
def comp_seq_5(seq_ref,seq_test): 
    for i in range(len(seq_test),0,-1): #i correspond à l'alignement (corect ou incorect)
        M=scoring_matrix(seq_test[-i:],seq_ref[0:i])          #remplis la matrice pour les 2 sequences
        score=M[-1][-1][0]                                    #le score se trouve dans la dernière case
        if score>=i-4:                                        #on retourne la matrice si le score est suffisament élevé
            return(M)                   
    return([[0]]) #sinon on retourne [[0]]

#Fonction permetant de chercher au sein d'une liste de chaines de caracteres L, la chaine de caractere la plus
#longue appelee maxseq (ce sera le contig), et retournant maxseq au format list apres l'avoir supprimee de la liste L initiale.
#Renvoyer [maxseq] au format list permet de pouvoir concatener [maxseq] a une autre liste "contig" introduite plus loin.
def max_length(L): 
    maxlen=0 #On introduit la variable maxlen qui correspond à la longueur la plus grande. Initialement elle vaut 0
    maxseq=0 #On introduit la variable maxseq qui correspond à la séquence avec la longueur la plus grande. 
    for i in L: #i correspond à une séquence (prises les unes après les autres) dans notre liste de read L
        if len(i)>maxlen: #Si la longueur de la séquence i est plus grande que maxlen précédent
            maxlen=len(i)   #Alors maxlen va prendre la valeur de la longeur de la séquence i 
            maxseq=i  #Et on stock dans maxseq cette séquence i
    L.remove(maxseq) #Une fois la plus grande séquence de toutes trouvée, on l'enlève de la liste de read 
    return([maxseq]) #Crochet pour renvoyer la séquence sous forme de liste 


#----------------------------L'INITIALISATION----------------------------------
# 1. On ouvre tout d'abord le fichier contenant l'ensemble des reads appele "my_read.txt" (adresse relative) qui doit se trouver au meme
#endroit que l'assembleur.
# 2. Puis la fonction read permet d'en extraire une chaine de caratere "brute"
# 3. Enfin, la fonction splitlines permet de scinder, a chaque retour a la ligne dans le fichier  "my_read.txt", cette chaine de caractere
#"brute" en plusieurs chaines de caracteres (les reads) que l'on stocke dans la liste appelée seq.
seq=open("read.txt").read().splitlines()
#On cree ensuite la liste contig qui ne contiendra au depart pour seul element, que la chaine de caractere la plus longue de la liste seq
#Cette plus longue chaine de caractere aura ete trouvee grace a la fonction max_length introduite plus haut.
contig=max_length(seq)

#--------------------------------L'ASSEMBLAGE-----------------------------------

while seq: #Tant que nous avons des séquences dans la liste seq i.e. tant que la liste seq n'est pas vide 
    best_overlap=0 #On définit une variable correspondant au meilleur alignement. Au début elle vaut 0
    best_read=0 #On définit une variable qui correspond à la séquence avec le meilleur alignement. Au début on a pas de séquence (0)
    #on compare, les unes après les autres, les sequences de seq avec le dernier contig de la liste contigs (au début il n'y a qu'un seul contig donc c'est bien le dernier...)
    for i in seq: #i correspond à une séquence dans seq 
        M_comp3=comp_seq_3(contig[-1],i) #variable contenant la matrice obtenue avec comp_seq_3
        M_comp5=comp_seq_5(contig[-1],i) #variable contenant la matrice obtenue avec comp_seq_5
        overlap_droite=len(M_comp3)-1 #l'overlap à droite correspond à la longueur de seq1/seq2(même longueur pour un assemblage...) donc c'est le nombre de ligne dans la matrice moins la première ligne (ca marcherais aussi avec les colonnes)
        overlap_gauche=len(M_comp5)-1 #l'overlap à gauche correspond à la longueur de seq1/seq2(même longueur pour un assemblage...) donc c'est le nombre de ligne dans la matrice moins la première ligne (ca marcherais aussi avec les colonnes)
        if max(overlap_droite,overlap_gauche)>best_overlap: #On prend la valeur max des overlap qu'on compare au best_overlap définit précedement. Et si il y a un des best_overlap qui est supérieur au best_overlap définit précédemment  alors:
            best_read=i #i devient la séquence avec le meilleur alignement et:
            if overlap_droite>=overlap_gauche:#Si l'alignement à droite est meilleur ou pareil que l'alignement à gauche (si il est pareil, on décide arbitrairement de prendre celui de droite)
                best_overlap=overlap_droite  #Alors le meilleur alignement est l'alignement à droite 
                coller="droite"  #On définit une variable qui nous dis où on doit coller la séquence test sur la séquence ref, dans le cas présent on la colle à droite de seq_ref 
                M=M_comp3 #si on colle a droite on garde en mémoire la matrice créée avec comp_seq_3, ça permettra de renvoyer le newseq plus tard
            else: #Si l'lignement à gauche est meilleur que l'alignement à droite 
                best_overlap=overlap_gauche #Alors le meilleur alignement est l'alignement à gauche 
                coller="gauche" #On crée alors une variable qui nous dit où on doit coller la séquence test sur la séquence de référence, dans le cas présent on la colle à gauche de seq_ref
                M=M_comp5 #Si on colle à gauche, on garde en mémoire la matrice créée avec comp_seq_5, ça permettra de renvoyer le newseq plus tard
    if best_overlap>0: #Une fois qu'on a comparé toutes les séquences de seq, si le meilleur alignement est plus grand que 0 (sinon c'est qu'il n'y a pas d'alignement correct) alors:
        newseq=matrix_to_newseq(M,best_read[0:best_overlap],contig[-1][-best_overlap:]) #on utilise la fonction matrix_to_newseq pour obtenir newseq que l'on intercale ensuite entre le contig et le read
        if coller=="droite": #Si précédemment on a trouvé que l'on devait coller la séquenece test à droite du contig:
            contig[-1]=contig[-1][0:-best_overlap]+newseq+best_read[best_overlap:] #On insère la séquence test(dont l'extremité 5' est remplacée par newseq) à droite de la séquence de référence
        elif coller=="gauche": #Si précédemment on a trouvé qu'on devait coller la séquence test à gauche du contig :
            contig[-1]=best_read[0:len(best_read)-best_overlap]+newseq+contig[-1][best_overlap:] #On insère la séquence test(dont l'extremité 3' est remplacée par newseq) à gauche de la séquence de référence 
        seq.remove(best_read) #On enlève cette séquence ajoutée du read 

    elif best_overlap==0 and seq!=[]:  #Si on trouve pas d'alignement et que seq n'est pas vide, il reste encore des sequences...
        contig=contig+max_length(seq)  #...donc on va créer un nouveau contig: on ajoute dans la liste de contig une nouvelle séquence qui correspond à la plus grande séquence restant dans la liste de read. Ce nouveau contig se retrouvera en position [-1] de la liste contig. Donc la suite de l'assemblage se fera dessus.

#---------------------------------------LES-RESULTATS-------------------------

print(contig)

#-------------------------------LIMITES --------------------------------------
#Ce programme, contrairement au précédent, prend en compte les mutations éventuelles.
#Pour le réaliser, nous nous sommes appuyés sur le l'algorithme de Needleman-Wunsch

#Cependant, le programme est très lent. En effet, l'algorithme de Needleman-Wunsch fait appel à la programation dynamique. Pour chaque comparaison seq_ref/seq_test, on réalise une
#certain nombre de mmatrice ce qui prend du temps.
#De plus, lorsqu'il y a une potentielle mutation, il faudrait pouvoir renvoyer les plusieurs contigs possibles (on peut considérer que la mutation est sur le contig ou bien sur le read) on a décidé arbitrairement sur quel brin se trouve la mutation sans renvoyer ces plusieurs contigs possibles. Pour ce
#qui est des mésapariements, on pourait utiliser un alphabet ambigu au lieu d'utiliser simplement ATGC e.g. Y pou T ou C
#Enfin, le programmea tendance à placer des mutations à la fin des reads lorsqu'il ne le faut pas... Comme l'algorithme utilise une fenêtre de lecture, il y a des erreurs en début et en fin de read là où la fenêtre est incomplète . En fait
#on pourait résoudrece programme en affinant la fonction de scoring: par exemple en demandant un score plus élevé si l'overlap est court.
#Pour conclure, on peut dire que ce programme est loin d'être parfait. Mais d'autres methodes permettent de produire de très bon assemblements de novo par l'utilisation de graphes
#De Bruijn par exemple.
